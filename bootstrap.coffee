module.exports = (User, Cnae, Job) ->

  names = ['Erasmo', 'Joana', 'Micaela', 'Paula', 'Matheus', 'Ash', 'Goku', 'Allejo', 'Augusto', 'Francisco', 'Xena', 'Judite', 'Luís']
  surnames = ['Constância', 'Alves', 'Jabor', 'Luz', 'Rodrigues', 'Muniz', 'Ketchum', 'Fernandes', 'Chaves', 'Silva', 'Obama', 'Roussef']
  jobs = ['Pintura de 3 cômodos', 'Produção de vídeo', 'Cartão de Visita', 'Criação de logo', 'Elaboração de Jornal', 'Reforma de Canil', 'Formatação de PC', 'Nivelamento de Piso']

  generateRandomPoint = (radius) ->
    y0 = -19.8619459
    x0 = -43.9593134
    # Convert Radius from meters to degrees.
    rd = radius / 111300
    u = Math.random()
    v = Math.random()
    w = rd * Math.sqrt(u)
    t = 2 * Math.PI * v
    x = w * Math.cos(t)
    y = w * Math.sin(t)
    xp = x / Math.cos(y0)
    # Resulting point.
    [xp + x0, y + y0]

  createJobsFor = (user, cnaes) ->
    job = new Job()
    job.title = jobs[Math.floor(Math.random() * 100) % jobs.length]
    job.description = "Esse é um serviço fictício criado para fins de demonstração. Assim que o aplicativo MeuMEI for aprovado os serviços de demonstração serão removidos do sistema para que os usuários de verdade possam utilizá-lo."
    job.location = user.location
    job.creator_photo = user.photo_url
    job.creator_id = user._id
    job.creator_name = user.name
    cnae = cnaes[Math.floor(Math.random() * 100) % cnaes.length]
    job.cnae_code = cnae.code
    job.cnae_name = cnae.name
    console.log job
    job.save()

  saveUser = (cnaes, count) ->
    user = new User()
    user.facebook_id = 'facebookid' + count
    user.name = names[Math.floor(Math.random() * 100) % names.length] + ' ' + surnames[Math.floor(Math.random() * 100) % surnames.length]
    user.email = 'usuario' + count + '@meumei.com'
    user.photo_url = 'https://api.adorable.io/avatars/200/' + user.email + '.io.png'
    user.isMei = Math.random() > 0.65
    user.isValidated = true
    user.accessToken = 'token_de_acesso'+count
    user.location = generateRandomPoint(10000)
    user.competences = []
    if(user.isMei)
      user.competences.push cnae for cnae in cnaes when Math.random() > 0.6
    user.save (error) ->
      createJobsFor user, cnaes unless user.isMei


  run: ->
    Cnae.find().exec (error, cnaes)->
      saveUser cnaes, count for count in [1..35]
