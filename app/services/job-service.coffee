module.exports = (Job, User, config) ->
  class JobService
    queryNearby: (query, callback) ->
      Job.QueryNearby query, callback

    getNearby: (query, callback) ->
      User.findById query.user, (error, user) ->
        if user
          Job.QueryNearby query, user, callback ->
            callback {success: true, result: available_jobs}

    accept: (job, user, callback) ->
      Job.findById job, (error, result) ->
        console.log result
        result.candidates = [] if result.candidates == undefined
        result.candidates.push user
        result.save (error) ->
          throw error if error
          callback {success: true, result: result}

    refuse: (job, user, callback) ->
      Job.findById job, (error, result) ->
        result.refuses = [] if result.refuses == undefined
        result.refuses.push user
        result.save (error) ->
          throw error if error
          callback {success: true, result: result}

    findByCreator: (user, callback) ->
      Job.findByCreator user, (result) ->
        callback result

    findByResponsible: (user, callback) ->
      console.log 'Buscando por usuário: ' + user
      Job.findByResponsible user, (result) ->
        callback result

    findCandidates: (job, callback) ->
      Job.findById job, (error, result) ->
        User.findInList result.candidates, (candidates) ->
          callback candidates

    refuseCandidate: (job_id, candidate, callback) ->
      Job.findById job_id, (error, job) ->
        job.refuses.push candidate
        index = job.candidates.indexOf candidate
        job.candidates.splice index, 1
        job.save (error) ->
          throw error if error
          callback job

    acceptCandidate: (job_id, candidate, callback) ->
      Job.findById job_id, (error, job) ->
        User.findById candidate, (error, user) ->
          job.responsible_id = user._id
          job.responsible_name = user.name
          job.responsible_photo = user.photo_url
          job.available = false
          job.save (error) ->
            throw error if error
            callback job

    getFeedbacks: (userId, callback) ->
      Job.find
        responsible_id: userId,
        rating:
          $gt: -1
      , (error, jobs) ->
        callback jobs

  return new JobService()
