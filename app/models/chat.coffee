module.exports = (mongoose) ->
  Schema = mongoose.Schema
  ChatSchema = new Schema(
    job: String
    message: String
    date: {type: Date, default: new Date()}
    sender_name: String
    sender_id: String
    sender_photo: String
  )

  ChatSchema.statics.countMessages = (job, callback) ->
    this.count( job: job ).exec (error, count) ->
      console.log count
      throw error if error
      callback count

  ChatSchema.statics.findByJob = (job, callback) ->
    this.find job: job, (error, messages) ->
      throw error if error
      callback messages

  ChatSchema.statics.addMessage = (chat, message, callback) ->
    chat.job = message.job
    chat.message = message.message
    chat.sender_name = message.user.name
    chat.sender_id = message.user._id
    chat.sender_photo = message.user.photo_url
    chat.save (error) ->
      throw error if error
      callback chat

  return mongoose.model 'Chat', ChatSchema
