module.exports = (mongoose) ->
  Schema = mongoose.Schema
  UserSchema = new Schema(
    name: String
    email: String
    facebook_id: String
    isMei: Boolean
    photo_url: String
    isValidated: Boolean
    birthday: String
    competences: Array
    accessToken: String
    location: Object
    bio: String
    last_online: Date
  )

  #Métodos estáticos
  UserSchema.statics.getByFacebookID = (facebook_id, callback) ->
    this.findOne( facebook_id: facebook_id ).exec (error, user) ->
      throw error if error
      callback user

  UserSchema.statics.GetMeiCount = (callback) ->
    this.count( isMei: true ).exec (error, count) ->
      console.log count
      throw error if error
      callback count

  UserSchema.statics.GetNonMeiCount = (callback) ->
    this.count( isMei: false ).exec (error, count) ->
      console.log count
      throw error if error
      callback count

  UserSchema.statics.findInList = (id_list, callback) ->
    id_list.map (id) ->  mongoose.Types.ObjectId(id)
    this.find
      _id:
        $in: id_list
    , (error, result) ->
      callback result

  return mongoose.model 'User', UserSchema
