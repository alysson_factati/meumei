module.exports = (mongoose) ->
  Schema = mongoose.Schema
  JobSchema = new Schema(
    title: String
    description: String
    location: {type: [Number], index: '2dsphere'}
    cnae_code: String
    cnae_name: String
    status: String
    date: {type: Date, default: Date.now}
    rating: {type: Number, default: -1}
    creator_id: String
    creator_name: String
    creator_photo: String
    responsible_id: String
    responsible_name: String
    responsible_photo: String
    available: {type: Boolean, default: true},
    candidates: {type: [String], default: []},
    refuses: {type: [String], default: []}
  )

  JobSchema.statics.GetAvaliableCount = (callback) ->
    this.count( available: true ).exec (error, count) ->
      console.log count
      throw error if error
      callback count

  JobSchema.statics.GetFinalizedCount = (callback) ->
    this.count( available: false ).exec (error, count) ->
      console.log count
      throw error if error
      callback count

  JobSchema.statics.GetNearby = (lat, lng, distance, callback) ->
    point = { type: "Point", coordinates: [ lng, lat ] }
    this.geoNear point, {maxDistance: distance, spherical: true} , (error, jobs) ->
      throw error if error
      callback jobs

  JobSchema.statics.findByCreator = (user, callback) ->
    this.find creator_id: user, (error, jobs) ->
      throw error if error
      callback jobs

  JobSchema.statics.findByResponsible = (user, callback) ->
    this.find responsible_id: user, (error, jobs) ->
      throw error if error
      callback jobs

  JobSchema.statics.QueryNearby = (query, user, callback) ->
    point = { type: "Point", coordinates: [query.lng, query.lat] }
    this.aggregate [
      {$geoNear:
        near: point
        maxDistance: query.distance
        spherical: true,
        distanceField: 'dis'}
      {$match:
        available: true
        candidates:
          $ne: user._id
        refuses:
          $ne: user._id}
    ]
    .exec (error, results) ->
      console.log error
      console.log results
      callback results

  return mongoose.model 'Job', JobSchema
