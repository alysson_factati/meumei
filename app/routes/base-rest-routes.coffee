module.exports = (express) ->
  class RESTRoute
    @router
    getRoute: ->
      @router

    createModel: (model, callback)->
      message = null
      model.save (err) ->
        if err
          if err.code == 11000
            message =
              success: false
              message: 'Já existe um registro correspondente no banco de dados.'
              error: err
          else
            message = err
        message =
          message: 'Registro criado!'
        callback model

    updateModel: (model, callback)->
      console.log "Atualizando registro..."
      console.log model
      model.save (error) ->
        if error
          callback error
        else
          message =
            message: 'Model updated!'
          callback model

    constructor: (Model, route)->
      @router = express.Router()
      rest = this;
      @router.route('/'+route).post (req, res) ->
        model = new Model
        model[field] = req.body[field] for field in rest.createFields
        rest.createModel model, (message) -> res.json(message)

      .get (req, response) ->
        console.log "Obtendo " + route
        Model.find (error, models) ->
          response.send error if error
          response.json models

      @router.route('/'+route+'/:model_id')
      .get (req, res) ->
        Model.findById req.params.model_id, (error, model) ->
          res.send error if error
          res.json model
      .put (req, res) ->
        Model.findById req.params.model_id, (error, model) ->
          if error
            res.json error
          else
            model[field] = req.body[field] for field in rest.updateFields when req.body[field] isnt undefined
            rest.updateModel model, (message) -> res.json(message)


      .delete (req, res) ->
        Model.remove _id: req.params.model_id, (error, model) ->
          return error if error
          res.json message: 'Model removed!'

  return RESTRoute
