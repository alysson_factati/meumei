module.exports = (express, jobService) ->
  nearbyRouter = express.Router()

  nearbyRouter.post '/jobs/:distance', (req, res) ->
    data =
      lat: parseFloat(req.body?.lat or 1)
      lng: parseFloat(req.body?.lng or 1)
      distance: parseInt(req.params.distance)
      user: req.body?.user
    console.log data
    jobService.getNearby data, (data) ->
      console.log data.result.length + " serviços encontrados"
      res.json data

  nearbyRouter.get '/teste', (req, res) ->
    data =
      lat: -19.863349
      lng: -43.957705999999995
      distance: 1000
      user: '571e0cdaa14efe0300b41444'
    console.log data
    jobService.queryNearby data, (data) ->
      console.log data.result + " serviços encontrados"
      res.json data

  nearbyRouter.post '/accept', (req, res) ->
    user = req.body?.user
    job =  req.body?.job
    console.log "Buscando serviço: " + job
    jobService.accept job, user, (data) ->
      console.log data
      res.json data

  nearbyRouter.post '/refuse', (req, res) ->
    user = req.body?.user
    job =  req.body?.job
    jobService.refuse job, user, (data) ->
      console.log data
      res.json data

  return nearbyRouter
