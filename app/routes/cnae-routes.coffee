module.exports = (RESTRoute, Cnae) ->
  class CnaeRoutes extends RESTRoute


    addRoutes: ->
      this.getRoute().get '/teste/34', (req, res)->
        console.log "Criando um aleatório"
        res.send 'Ae'

    constructor: ->
      @createFields = ['code', 'name']
      @updateFields = ['code', 'name']
      super(Cnae, 'cnae')
      this.addRoutes()

  return new CnaeRoutes().getRoute()
