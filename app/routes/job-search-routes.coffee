module.exports = (express, jobService) ->
  jobSearchRouter = express.Router()

  jobSearchRouter.post '/creator', (req, res) ->
    userId = req.body?.user
    jobService.findByCreator userId, (data) ->
      console.log data.length + " serviços encontrados"
      res.json data

  jobSearchRouter.post '/responsible', (req, res) ->
    userId = req.body?.user
    jobService.findByResponsible userId, (data) ->
      console.log data.length + " serviços encontrados"
      res.json data

  jobSearchRouter.post '/candidates', (req, res) ->
    jobId = req.body?.job
    console.log "Procurando candidatos para " + jobId
    jobService.findCandidates jobId, (candidates) ->
        res.json candidates

  jobSearchRouter.post '/refuse-candidate', (req, res) ->
    jobId = req.body?.job
    candidateId = req.body?.candidate
    console.log "Candidato " + candidateId + " reprovado para " + jobId
    jobService.refuseCandidate jobId, candidateId, (job) ->
        res.json job

  jobSearchRouter.post '/accept-candidate', (req, res) ->
    jobId = req.body?.job
    candidateId = req.body?.candidate
    console.log "Candidato " + candidateId + " aprovado para " + jobId
    jobService.acceptCandidate jobId, candidateId, (job) ->
        res.json job

  jobSearchRouter.post '/feedback', (req, res) ->
    userId = req.body?.user
    console.log "Obtendo feedbacks para " + userId
    jobService.getFeedbacks userId, (jobs) ->
        res.json jobs

  return jobSearchRouter
