module.exports = (express, meiService) ->
  meiRouter = express.Router()

  meiRouter.get '/captcha', (req, res) ->
    console.log "Captcha"
    meiService.getCaptcha (data)->
      res.json data

  meiRouter.post '/validate', (req, res) ->
    meiService.validateMei req.body, (data) ->
      res.json data

  return meiRouter
