module.exports = (express, dashboardService) ->
  console.log "Criando Rotas do MEI"
  dashboardRouter = express.Router()

  dashboardRouter.get '/', (req, res) ->
    dashboardService.getInfo (info) ->
      res.json info

  return dashboardRouter
