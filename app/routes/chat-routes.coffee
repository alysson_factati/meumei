module.exports = (express, Chat, io) ->
  chatRouter = express.Router()

  chatRouter.get '/:job_id', (req, res, next) ->
    Chat.findByJob req.params.job_id, (messages) ->
      res.json messages

  io.on 'connection', (socket) ->
    console.log "Usuário conectado!"
    socket.on 'chat_message', (msg) ->
      console.log "Mensagem recebida!"
      console.log msg
      Chat.addMessage new Chat(), msg, (chat) ->
        io.emit chat.job, chat

  return chatRouter
